﻿using BaseLinkerConnection;
using BaseLinkerConnection.BaseLinker;
using BaseLinkerConnection.BaseLinker.Bidings;
using BaseLinkerConnection.Shoper.Bindings;
using BaseLinkerConnection.SkyShop;
using BaseLinkerConnection.SkyShop.Bindings;
using CommonToolSet.Bindings;
using CommonToolSet.Interfaces;
using CommonToolSet.OutPuts;
using Flow.SeriesComparer;
using Flow.SeriesComparer.Bindings;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECommerceComparer
{
    class Program
    {
        

        static void Main(string[] args)
        {

            using (IKernel kernel = new StandardKernel(new LoggerNinjectBinding(),
                                   new BaseLinkerSeriesProviderNinject(),new SkyShopKnownStatusesNinject(),
                                   new SeriesComparerNinject(), new DBConnectionNinject()))
            {
                
                SeriesComparer seriesComparer = kernel.Get<SeriesComparer>();
                IDBConnection dB    = kernel.Get<IDBConnection>();
                ILogger logger = kernel.Get<ILogger>();
                //SkyShopConnector connector = new SkyShopConnector(logger);
                //connector.GetResponse("getOrderStatuses", HttpMethods.GET);
                //seriesComparer.CompareSeries();


                BaseLinkerProducts baseLinkerProducts = new BaseLinkerProducts(logger);
                var key = (JObject)baseLinkerProducts.GetProducts("getProductsList", HttpMethods.POST);
                Console.WriteLine(key);
            }
        }
    }
}
