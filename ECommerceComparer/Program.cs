﻿using BaseLinkerConnection.BaseLinker.Bidings;
using CommonToolSet.Bindings;
using Erp;
using Erp.Bindings;
using Ninject;

namespace ECommerceComparer
{
    class Program
    {
        static void Main(string[] args)
        {

            string config = Properties.Resources.Config;

            using (IKernel kernel = new StandardKernel(
                                   new FileLoggerNinject(),
                                   new ErpNinjectBinding(config),
                                   new BaseLinkerProductsProviderNinject()
                                   ))
            {
                DataBase dataBase= kernel.Get<DataBase>();
                
                dataBase.WriteData();
                dataBase.ReadData();
                dataBase.DeleteData();

                //var dataReader = kernel.Get<ReadingFromDatabase>();
                //dataReader.ReadData();
            }
        }
    }
}
