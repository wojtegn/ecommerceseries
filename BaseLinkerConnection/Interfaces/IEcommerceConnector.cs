﻿using Newtonsoft.Json.Linq;
using System;

namespace BaseLinkerConnection.Interfaces
{
    public interface IEcommerceConnector
    {
        Object GetResponse(string endpoint, HttpMethods method, String param = "{}");
    }
}
