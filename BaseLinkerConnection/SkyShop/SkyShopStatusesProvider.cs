﻿using BaseLinkerConnection.Interfaces;
using BaseLinkerConnection.Shoper;
using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace BaseLinkerConnection.SkyShop

{
    class SkyShopStatusesProvider : ISeriesProvider
    {
        IEcommerceConnector _ecommerceConnector;
        ILogger _logger;

        public SkyShopStatusesProvider(IEcommerceConnector iECommerceConnector, ILogger logger)
        {
            _ecommerceConnector = iECommerceConnector;
            _logger = logger;
        }

        public IEnumerable<ISeries> GetSeries()
        {
            XElement response = (XElement)_ecommerceConnector.GetResponse("getOrderStatuses", HttpMethods.GET);

            var statuses = response.Element("statuses").Elements().ToList();
            List<SkyShopStatuses> skyShopStatuses = new List<SkyShopStatuses>();
            foreach (var status in statuses)
            {
                SkyShopStatuses skyShop = new SkyShopStatuses(status.Value);
                skyShopStatuses.Add(skyShop);
            }

            return skyShopStatuses;
        }
    }
}
