﻿using Flow.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.SkyShop
{
    class SkyShopStatuses : ISeries
    {
        public int Id { get; }
        public string Name { get; }

        public SkyShopStatuses(string name)
        {
            Name = name;
        }
    }
}
