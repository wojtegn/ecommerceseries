﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace BaseLinkerConnection.SkyShop
{
    public class SkyShopConnector : IEcommerceConnector
    {
        ILogger _logger;

        public SkyShopConnector(ILogger logger)
        {
            _logger = logger;
        }

        public Object GetResponse(string endpoint, HttpMethods method, String param = "{}")
        {
            _logger.wirteDebug("GetResponse called");
            String APIKey = "4882c708";
            string format = "xml";
            string response = "";
            string URIAdress = "http://trailerpark.mysky-shop.pl/";
            // getOrderStatuses&APIkey=4882c708&format=xml
            string parameters = $"api/?function={endpoint}&APIkey={APIKey}&format={format}";


            XElement xElement;
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.Encoding = Encoding.UTF8;

                    switch (method)
                    {
                        case HttpMethods.GET:
                            response = webClient.DownloadString(URIAdress + parameters);
                            xElement = XElement.Parse(response);
                            break;
                        default:
                            return null;
                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return xElement;
        }
    }
}
