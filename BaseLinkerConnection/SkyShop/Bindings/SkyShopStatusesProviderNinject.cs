﻿using BaseLinkerConnection.Interfaces;
using Flow.Domain.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.SkyShop.Bindings
{
    public class SkyShopStatusesProviderNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<ISeriesProvider>().To<SkyShopStatusesProvider>();
            Bind<IEcommerceConnector>().To<SkyShopConnector>();
        }
    }
}
