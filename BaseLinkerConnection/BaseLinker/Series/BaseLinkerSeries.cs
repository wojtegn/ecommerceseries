﻿using Flow.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker
{
    public class BaseLinkerSeries : ISeries
    {
        int _Id;
        string _Name;
        public int Id
        {
            get => _Id;
        }
        public string Name
        {
            get => _Name;
        }
        public BaseLinkerSeries(int Id, string Name)
        {
            _Id = Id;
            _Name = Name;
        }
    }
}
