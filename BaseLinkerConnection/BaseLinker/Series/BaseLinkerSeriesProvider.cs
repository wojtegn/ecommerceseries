﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker
{
    public class BaseLinkerSeriesProvider : ISeriesProvider
    {
        IEcommerceConnector _ecommerceConnector;
        ILogger _logger;
        public BaseLinkerSeriesProvider(IEcommerceConnector ecommerceConnector, ILogger logger)
        {
            _ecommerceConnector = ecommerceConnector;
            _logger = logger;
        }
        public IEnumerable<ISeries> GetSeries()
        {
            JObject jObject = (JObject)_ecommerceConnector.GetResponse("getSeries", HttpMethods.POST);
            JArray keys = (JArray)jObject["series"];
            List<ISeries> listOfSeries = new List<ISeries>();
            foreach(var k in keys)
            {
                int id;
                string name = k["name"].ToString();
                int.TryParse(k["id"].ToString(), out id);
                BaseLinkerSeries baseLinkerSeries = new BaseLinkerSeries(id, name);
                listOfSeries.Add(baseLinkerSeries);
            }
            
           
            return listOfSeries;
        }
    }
}
