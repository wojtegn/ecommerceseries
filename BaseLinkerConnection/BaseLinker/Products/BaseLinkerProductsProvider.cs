﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker.Products
{
    class BaseLinkerProductsProvider : IProductsProvider
    {
        IEcommerceConnector _ecommerceConnector;
        ILogger _logger;
        public BaseLinkerProductsProvider(IEcommerceConnector ecommerceConnector, ILogger logger)
        {
            _ecommerceConnector = ecommerceConnector;
            _logger = logger;
        }
        public IEnumerable<IProduct> GetProducts()
        {
            String param = "{\"storage_id\": \"bl_1\"}";
            JObject response = (JObject)_ecommerceConnector.GetResponse("getProductsList", HttpMethods.POST, param);
            JArray products = (JArray)response["products"];
            List<IProduct> listOfProducts = new List<IProduct>();
            foreach (var item in products)
            {
                string ean = item["ean"].ToString();
                string sku = item["sku"].ToString();
                string name =  item["name"].ToString();
                string quantity = item["quantity"].ToString();
                int quant;
                int.TryParse(quantity, out quant);

                BaseLinkerProducts baseLinkerProducts = new BaseLinkerProducts(name, sku, ean, quant);
                listOfProducts.Add(baseLinkerProducts);
            }

            
            

            return listOfProducts;
        }
    }
}
