﻿using Flow.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker.Products
{
    class BaseLinkerProducts : IProduct
    {
        public string Name { get; }

        public string SKU { get; }

        public string EAN { get; }

        public int Quantity { get; }

        public BaseLinkerProducts(string name, string sku, string ean, int quantity)
        {
            Name = name;
            SKU = sku;
            EAN = ean;
            Quantity = quantity;
        }
    }
}
