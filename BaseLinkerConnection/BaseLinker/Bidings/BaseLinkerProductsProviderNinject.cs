﻿using BaseLinkerConnection.BaseLinker.Products;
using BaseLinkerConnection.Interfaces;
using Flow.Domain.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker.Bidings
{
    public class BaseLinkerProductsProviderNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<IEcommerceConnector>().To<BaseLinkerConnector>();
            Bind<IProductsProvider>().To<BaseLinkerProductsProvider>();
        }
    }
}
