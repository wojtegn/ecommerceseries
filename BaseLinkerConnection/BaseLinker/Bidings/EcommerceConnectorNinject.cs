﻿using BaseLinkerConnection.BaseLinker;
using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.Bidings
{
   public class EcommerceConnectorNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<IEcommerceConnector>().To<BaseLinkerConnector>();
        }
    }
}
