﻿using BaseLinkerConnection.Interfaces;
using Flow.Domain.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.BaseLinker.Bidings
{
    public class BaseLinkerSeriesProviderNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<ISeriesProvider>().To<BaseLinkerSeriesProvider>();
            Bind<IEcommerceConnector>().To<BaseLinkerConnector>();
        }
    }
}
