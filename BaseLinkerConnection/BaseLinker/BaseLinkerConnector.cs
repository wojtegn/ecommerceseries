﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Newtonsoft.Json.Linq;
using System;
using System.Net;

namespace BaseLinkerConnection.BaseLinker
{
    internal class BaseLinkerConnector : IEcommerceConnector
    {
        ILogger _logger;

        public BaseLinkerConnector(ILogger logger)
        {
            _logger = logger;
        }
        public Object GetResponse(string endpoint, HttpMethods method, String param)
        {
            _logger.wirteDebug("GetResponse called");
            String token = "2000587-2002861-A6LLBOP61EN6T7XD7W852FAGZMNGUA862YIOZLSJA2ZNZ3TCNFNW3IT5JBOYNL4Q";
            


            string URIAdress = "https://api.baselinker.com/connector.php";
            string parameters = "token=" + token + "&method=" + endpoint + "&parameters=" + param;
            JObject jObject = new JObject();


            try
            {
                using (WebClient webClient =  new WebClient())
                {
                    webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string result = webClient.UploadString(URIAdress, parameters);
                    _logger.writeInfo("Rozpoczynam pobieranie danych");

                    jObject = JObject.Parse(result);
                }
            }
            catch (WebException ex)
            {

                _logger.wirteDebug("GetResponse failed. Reason: " + ex.Message);
                _logger.writeError("Nie można pobrać informacji z systemu Ecommerce. Powód: " + ex.Message);
            
            }
            catch (Exception ex)
            {
                _logger.wirteDebug($"Unknow error: {ex.Message}");
                _logger.writeError($"Nieznany błąd: {ex.Message}");
            }
            return jObject;
        }
    }
}
