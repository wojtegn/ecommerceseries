﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.Shoper
{
    class ShoperSeriesProvider : ISeriesProvider
    {
        IEcommerceConnector _ecommerceConnector;
        ILogger _logger;

        public ShoperSeriesProvider(IEcommerceConnector ecommerceConnector, ILogger logger)
        {
            _ecommerceConnector = ecommerceConnector;
            _logger = logger;
        }

        public IEnumerable<ISeries> GetSeries()
        {
            var res = (JObject)_ecommerceConnector.GetResponse("webapi/rest/statuses", HttpMethods.GET);
            JArray list = (JArray)res["list"];
            List<ShoperSeries> listOfTranslations = new List<ShoperSeries>();
            int id = 0;
            string Name = "";
            foreach(var k in list)

            {              
                var keys = k["translations"];
                var key = keys["pl_PL"];
                Name = key["name"].ToString();
                string status = key["status_id"].ToString();
                int.TryParse(status, out id);
                ShoperSeries shoperSeries = new ShoperSeries(id, Name);
                listOfTranslations.Add(shoperSeries);
            }
           
            return listOfTranslations;
        }

    }
}
