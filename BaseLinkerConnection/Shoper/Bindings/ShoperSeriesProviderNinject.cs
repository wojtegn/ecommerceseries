﻿using BaseLinkerConnection.Interfaces;
using Flow.Domain.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.Shoper.Bindings
{
    public class ShoperSeriesProviderNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<ISeriesProvider>().To<ShoperSeriesProvider>();
            Bind<IEcommerceConnector>().To<ShoperConnector>();
        }
    }
}
