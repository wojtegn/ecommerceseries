﻿using Flow.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.Shoper
{
    class ShoperSeries : ISeries
    {
        public int Id { get; }
        public string Name { get; }

        public ShoperSeries(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
