﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Text;

namespace BaseLinkerConnection.Shoper
{
    class ShoperConnector : IEcommerceConnector
    {
        private string password = "Test123!@#";
        private string user = "admin";
        private string baseUrl = @"https://devshop-793460.shoparena.pl/";
        private string base64Password;
        private ILogger _logger;
        public ShoperConnector(ILogger logger)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | (SecurityProtocolType)768 /* SecurityProtocolType.Tls11 */ | (SecurityProtocolType)3072 /*SecurityProtocolType.Tls12*/;
            byte[] byteArray = new UTF8Encoding().GetBytes(String.Format("{0}:{1}", user, password));
            base64Password = Convert.ToBase64String(byteArray);
            _logger = logger;
        }

        public Object GetResponse(string endpoint, HttpMethods method, String param = "{}")
        {
            return getJresponse(endpoint, method, AuthorizationMethod.Bearer);
        }

        private JObject getJresponse(string endpoint, HttpMethods method, AuthorizationMethod authorizationMethod)
        {
            JObject toReturn = null;
            string response = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers[HttpRequestHeader.Accept] = "application/json";
                    client.BaseAddress = baseUrl;

                    switch (authorizationMethod)
                    {
                        case AuthorizationMethod.Bearer:
                            client.Headers[HttpRequestHeader.Authorization] = $"Bearer {getToken()}";
                            break;
                        case AuthorizationMethod.Basic:
                            client.Headers[HttpRequestHeader.Authorization] = $"Basic {base64Password}";
                            break;
                        default:
                            client.Headers[HttpRequestHeader.Authorization] = $"Basic {base64Password}";
                            break;
                    }

                    switch (method)
                    {
                        case HttpMethods.GET:
                            response = client.DownloadString(baseUrl + endpoint);
                            break;
                        case HttpMethods.POST:
                            response = client.UploadString(baseUrl + endpoint, "");
                            break;
                    }
                    toReturn = JObject.Parse(response);
                }
                catch (WebException ex)
                {
                    _logger.writeError($"Wystąpił błąd. Powód: {ex.Message}");
                }
                catch (Exception e)
                {
                    _logger.writeError($"Nieznany błąd {e.Message}");
                }
            }

            toReturn = JObject.Parse(response);
            return toReturn;
        }

        private string getToken()
        {
            var res = getJresponse("webapi/rest/auth", HttpMethods.POST, AuthorizationMethod.Basic);
            return res["access_token"].ToString();
        }
    }

    enum AuthorizationMethod
    {
        Bearer,
        Basic
    }
}
