﻿using BaseLinkerConnection.Interfaces;
using CommonToolSet.OutPuts;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.FakeBaseLinker
{
    internal class FakeBaseLinkerConnector : IEcommerceConnector
    {
        ILogger _logger;
        public FakeBaseLinkerConnector(ILogger logger)
        {
            _logger = logger;
        }

        public Object GetResponse(string endpoint, HttpMethods method, String param = "{}")
        {
            string data = "{\"series\": [{\"id\": \"77257\",\"name\": \"Miesięczna\"},]}";
            return JObject.Parse(data);
        }
    }
}
