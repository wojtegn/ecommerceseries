﻿using BaseLinkerConnection.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLinkerConnection.FakeBaseLinker.Bindings
{
    public class FakeBaseLinkerNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<IEcommerceConnector>().To<FakeBaseLinkerConnector>();
        }
    }
}
