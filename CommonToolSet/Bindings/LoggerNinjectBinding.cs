﻿using CommonToolSet.OutPuts;
using Ninject.Modules;

namespace CommonToolSet.Bindings
{
    public class LoggerNinjectBinding : NinjectModule
    {
        public override void Load()
        {
            Bind<OutPuts.ILogger>().To<OutPuts.Logger>();
            Bind<IOutPutFormatter>().To<OutputFormatter>();
        }
    }
}
