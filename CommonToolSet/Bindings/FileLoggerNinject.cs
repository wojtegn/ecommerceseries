﻿using CommonToolSet.OutPuts;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonToolSet.Bindings
{
    public class FileLoggerNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<IOutPutFormatter>().To<OutputFormatter>();
            Bind<ILogger>().To<FileLogger>();
        }
    }
}
