﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonToolSet.OutPuts
{
    public interface ILogger
    {
        void writeInfo(string info);
        void writeError(string info);
        void wirteDebug(string info);
        

    }
}
