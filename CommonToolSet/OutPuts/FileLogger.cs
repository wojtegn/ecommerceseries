﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CommonToolSet.OutPuts
{
    class FileLogger : ILogger
    {
        IOutPutFormatter _outPutFormatter;

        public FileLogger(IOutPutFormatter outPutFormatter)
        {
            _outPutFormatter = outPutFormatter;
        }
        public void wirteDebug(string info)
        {
            log(info, FormatOption.debug);
        }

        public void writeError(string info)
        {
            log(info, FormatOption.error);
        }

        public void writeInfo(string info)
        {
            log(info, FormatOption.info);
        }

        private void log(string info, FormatOption formatOption)
        {
            using (StreamWriter writer = new StreamWriter("logs.txt", append: true))
            {
                writer.WriteLine(_outPutFormatter.FormatOutput(info, formatOption));
            }
        }

    }
}
