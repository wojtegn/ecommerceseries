﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonToolSet.OutPuts
{
    public interface IOutPutFormatter
    {
        string FormatOutput(string info, FormatOption formatOption);

    }
}
