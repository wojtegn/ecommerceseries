﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonToolSet.OutPuts
{
    internal class OutputFormatter : IOutPutFormatter
    {
        public string FormatOutput(string info, FormatOption formatOption)
        {
            switch (formatOption)
            {
                case FormatOption.info:
                    return $"#User info " + info;
                case FormatOption.error:
                    return $"#User error {info}";
                case FormatOption.debug:
                    return $"#Debug "+ info;
                default:
                    return $"#Debug" + info;
            }
        }
    }
}
