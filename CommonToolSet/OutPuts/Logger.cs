﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonToolSet.OutPuts
{
    internal class Logger : ILogger
    {
        IOutPutFormatter _outputFormatter;

        public Logger(IOutPutFormatter outputFormatter)
        {
            _outputFormatter = outputFormatter;
        }

        public void writeInfo(string info)
        {
            log(info, FormatOption.info);
        }

        public void writeError(string info)
        {
            log(info, FormatOption.error);
        }

        public void wirteDebug(string info)
        {
            log(info, FormatOption.debug);
        }

       

        private void log(string info, FormatOption formatOption)
        {
            Console.WriteLine(_outputFormatter.FormatOutput(info,  formatOption));
        }

    }
}
