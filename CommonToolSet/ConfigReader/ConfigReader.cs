﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CommonToolSet.ConfigReader
{
    public static class ConfigReader
    {
        public static string GetStringFromConfig(XElement xElement, string node, string defaultValue = "")
        {
            var result = xElement.Element(node)?.Value ?? defaultValue;
            return result;
        }

        public static bool GetBoolFromConfig(XElement xElement, string node)
        {
            var result = xElement.Element(node)?.Value == "true"  ? true : false;
            return result;
        }
    }
}
