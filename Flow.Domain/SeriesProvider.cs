﻿using BaseLinkerConnection.Bidings;
using BaseLinkerConnection.Interfaces;
using CommonToolSet.Bindings;
using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Domain
{
    public class SeriesProvider : ISeriesProvider
    {
        public IEnumerable<ISeries> GetSeries()
        {
            using (IKernel kernel = new StandardKernel(new LoggerNinjectBinding(), new EcommerceConnectorNinject()))
            {
                IEcommerceConnector connector = kernel.Get<IEcommerceConnector>();
                JObject response = connector.GetResponse();
                List<ISeries> listOfSeries = new List<ISeries>();
                JArray keys = (JArray)response["series"];

                foreach(var k in keys)
                {
                    BaseLinkerSeries baseLinkerSeries = new BaseLinkerSeries()1
                    Console.WriteLine(k["name"]);
                }

                return null;

            }
        }
    }
}
