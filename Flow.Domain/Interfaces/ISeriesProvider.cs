﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Domain.Interfaces
{
    public interface ISeriesProvider
    {
        IEnumerable<ISeries> GetSeries();
    }
}
