﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Domain.Interfaces
{
    public interface ISeries
    {
        int Id { get; }

        string Name { get; }
    }
}
