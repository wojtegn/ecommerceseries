﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Domain.Interfaces
{
    public interface IProduct
    {
        string Name { get; }
        string SKU { get; }
        string EAN { get; }
        int Quantity { get; }
    }
}
