﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.SeriesComparer.Bindings
{
    public class SeriesComparerNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<SeriesComparer>().ToSelf();
        }
    }
}
