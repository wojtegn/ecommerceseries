﻿using CommonToolSet.OutPuts;
using Flow.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Flow.SeriesComparer
{
    public class SeriesComparer
    {
        private readonly ILogger _logger;
        private readonly ISeriesProvider _seriesProvider;
        List<string> _knownSeries = new List<string> { "Test", "przesyłka wysłana", "anulowane", "odrzucone", "zwrócone" };

        public SeriesComparer(ILogger logger, ISeriesProvider seriesProvider)
        {
            _logger = logger;
            _seriesProvider = seriesProvider;
        }

        public void CompareSeries()
        {
            _logger.writeInfo("Rozpoczynam porównywanie serii");
            _logger.writeInfo("Znane serie dokumentów to: " + string.Join(", ", _knownSeries));
            IEnumerable<ISeries> ecommerceSeries = _seriesProvider.GetSeries();

            List<string> compare = ecommerceSeries.Select(n => n.Name).ToList();
            _logger.writeInfo("Serie do porównania: " + string.Join(", ", compare));
            IEnumerable<ISeries> unknown = ecommerceSeries.Where(n => !_knownSeries.Any(x => x == n.Name)).ToList();
            List<string> difference = unknown.Select(n => n.Name).ToList();
            _logger.writeInfo("Nieznane serie to: "+ string.Join(", ", difference));

        }
    }
}
