﻿using Erp.SqlConnectionDataBuilder;
using Ninject.Modules;

namespace Erp.Bindings
{
    public class ErpNinjectBinding : NinjectModule
    {
        private readonly string _config;
        public ErpNinjectBinding(string config)
        {
            _config = config;
        }

        public override void Load()
        {
            Bind<ISqlConnectionStringProvider>().ToConstructor(x 
                                                => new SqlConnectionStringProvider(_config));

            Bind<DataBase>().ToSelf();
        }
    }
}
