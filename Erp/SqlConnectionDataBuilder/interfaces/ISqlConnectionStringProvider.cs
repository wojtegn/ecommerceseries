﻿namespace Erp.SqlConnectionDataBuilder
{
    public interface ISqlConnectionStringProvider
    {
        string GetSqlConnectionString();
    }
}