﻿using CommonToolSet.ConfigReader;
using System;
using System.Data.SqlClient;
using System.Xml.Linq;

namespace Erp.SqlConnectionDataBuilder
{
    internal class SqlConnectionStringProvider : ISqlConnectionStringProvider
    {
        private readonly XElement _xConfig;
        public SqlConnectionStringProvider(string configuration)
        {
            _xConfig = XElement.Parse(configuration);
        }

        public string GetSqlConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = getString("server") ?? throw new NullReferenceException("Server key was not provided");
            builder.InitialCatalog = getString("dataBase") ?? throw new NullReferenceException("Database key was not provided");

            if (getBool("isWindowsAuth"))
            {
                builder.IntegratedSecurity = true;
            }
            else
            {
                builder.IntegratedSecurity = false;
                builder.UserID = getString("dataBaseUser") ?? throw new NullReferenceException("DataBaseUser key was not provided");
                builder.Password = getString("dataBasePass");
            }

            return builder.ConnectionString;
        }

        string getString(string node)
        {
            return ConfigReader.GetStringFromConfig(_xConfig.Element("sqlConnection"), node, null);
        }

        bool getBool(string node)
        {
            return ConfigReader.GetBoolFromConfig(_xConfig.Element("sqlConnection"), node);
        }

    }
}
