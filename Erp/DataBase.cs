﻿using CommonToolSet.OutPuts;
using Erp.SqlConnectionDataBuilder;
using Flow.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Erp
{
    public class DataBase
    {
        private readonly Func<string, SqlConnection> _getConnection;
        private readonly ISqlConnectionStringProvider _sqlConnectionStringProvider;
        private readonly ILogger _logger;
        IProductsProvider _productsProvider;
        public DataBase(ISqlConnectionStringProvider sqlConnectionStringProvider, ILogger logger, IProductsProvider productsProvider)
        {
            _getConnection = (string connection) =>
            {
                return new SqlConnection(connection);
            };

            _sqlConnectionStringProvider = sqlConnectionStringProvider;
            _logger = logger;
            _productsProvider = productsProvider;
        }

        public void ReadData()
        {
            using (SqlConnection connection = _getConnection(
                                _sqlConnectionStringProvider.GetSqlConnectionString()))
            {


                connection.Open();

                string sqlQuery = @"SELECT *
                              FROM[BaseLinker].[dbo].[products]";

                using (SqlCommand cmd = new SqlCommand(sqlQuery, connection))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            _logger.writeInfo($"{dr["productName"]} \t {dr["sku"]} \t {dr["ean"]} \t {dr["quantity"]}");
                        }
                    }
                }
            }
        }
        public void WriteData()
        {
            string sql = "";
            IEnumerable<IProduct> products = _productsProvider.GetProducts();
            using (SqlConnection connection = _getConnection(_sqlConnectionStringProvider.GetSqlConnectionString()))
            {
                connection.Open();

                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    foreach (var product in products)
                    {
                        sql = @"INSERT INTO [BaseLinker].[dbo].[products] (productName, sku, ean, quantity)" + //Wpisz w tabelę products 
                                                    $" SELECT '{product.Name}','{product.SKU}','{product.EAN}',{product.Quantity} " // te wartości 
                                                    + $"WHERE NOT EXISTS (SELECT sku FROM products WHERE sku = '{product.SKU}')";   //jeżeli nic nie jest zwrócone przez tego selecta

                        adapter.InsertCommand = new SqlCommand(sql, connection);
                        adapter.InsertCommand.ExecuteNonQuery();
                    }



                }

            }



        }

        public void UpdateData()
        {
            string sql = "";
            IEnumerable<IProduct> products = _productsProvider.GetProducts();
            using (SqlConnection connection = _getConnection(_sqlConnectionStringProvider.GetSqlConnectionString()))
            {
                connection.Open();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    foreach (IProduct product in products)
                    {
                        sql = $"UPDATE products set productName = '{product.Name}', sku = '{product.SKU}', ean = '{product.EAN}', quantity = '{product.Quantity}'" +
                            $" WHERE sku = '{product.SKU}'";

                        adapter.InsertCommand = new SqlCommand(sql, connection);
                        adapter.InsertCommand.ExecuteNonQuery();
                        //_logger.wirteDebug(sql);
                    }
                }

            }


        }

        public void DeleteData()
        {
            string sql = "";
            IEnumerable<IProduct> products = _productsProvider.GetProducts();
            using (SqlConnection connection = _getConnection(_sqlConnectionStringProvider.GetSqlConnectionString()))
            {
                connection.Open();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    foreach (IProduct product in products)
                    {
                        sql = $"DELETE FROM products WHERE productName = '{product.Name}'";

                        adapter.InsertCommand = new SqlCommand(sql, connection);
                        adapter.InsertCommand.ExecuteNonQuery();
                        //_logger.wirteDebug(sql);
                    }
                }

            }


        }
    }
}
